# reconz
reconz is a tool to automate reconnaissance process, it includes gathering domain information, subdomain bruteforcing, subdomain probing, domain enumeration, screenshoting all subdomain pages, and vulnerability scanning. 
# usage
```
./reconz.sh example.com
```
# requirements
- linux
- zsh
- amass
- gospider
- whois
- httpx
- subdomainizer
- eyewitness 
- nuclei
- nuclei-templates (installed in ~/nuclei-templates)
# additional configuration
add below functions in ~/.zshrc file:
```
subdomainizer(){
    python3 ~/<your_subdomainizer_path>/SubDomainizer.py $@
}

eyewitness(){
    python3 ~/<your_eyewitness_path>/EyeWitness.py $@
}
```
# result
all of the recon result will be saved in a folder named the domain name e.g. example.com folder. 
