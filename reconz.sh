#!/bin/bash
DOMAIN=$1
FULL_DOMAIN="https://$DOMAIN" 
RED='\033[0;31m'
BLUE='\033[0;34m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
NC='\033[0m' # no color

mkdir $DOMAIN && cd $DOMAIN
cwd="$(pwd)"

# enable custom command (e.g. subdomainizer & eyewitness) from .zshrc file
source ~/.zshrc 2>/dev/null

if [ $# -eq 0 ]; then
	echo -e "${RED}you should write the targeted domain${NC}"
	exit 0
fi

# if [ isDomainNotValid($DOMAIN) ]; then
#	echo "you should write a valid domain (e.g. facebook.com)"
#	exit 0
# fi
echo ''
echo ''
echo ''
echo ' ██▀███  ▓█████ ▄████▄  ▒█████   ███▄    █ ▒███████▒'
echo '▓██ ▒ ██▒▓█   ▀▒██▀ ▀█ ▒██▒  ██▒ ██ ▀█   █ ▒ ▒ ▒ ▄▀░'
echo '▓██ ░▄█ ▒▒███  ▒▓█    ▄▒██░  ██▒▓██  ▀█ ██▒░ ▒ ▄▀▒░ '
echo '▒██▀▀█▄  ▒▓█  ▄▒▓▓▄ ▄██▒██   ██░▓██▒  ▐▌██▒  ▄▀▒   ░'
echo '░██▓ ▒██▒░▒████▒ ▓███▀ ░ ████▓▒░▒██░   ▓██░▒███████▒'
echo '░ ▒▓ ░▒▓░░░ ▒░ ░ ░▒ ▒  ░ ▒░▒░▒░ ░ ▒░   ▒ ▒ ░▒▒ ▓░▒░▒'
echo '  ░▒ ░ ▒░ ░ ░  ░ ░  ▒    ░ ▒ ▒░ ░ ░░   ░ ▒░░░▒ ▒ ░ ▒'
echo '  ░░   ░    ░  ░       ░ ░ ░ ▒     ░   ░ ░ ░ ░ ░ ░ ░'
echo '   ░        ░  ░ ░         ░ ░           ░   ░ ░    '
echo '               ░                           ░        '
echo '                                     by variousnabil'
echo ''
echo ''
echo '                          __________________________'
echo '                         <this recon is good, lads!>'
echo '                          --------------------------'
echo '                                \   ^__^'
echo '                                 \  (oo)\_______'
echo '                                    (__)\       )\/\'
echo '                                        ||----w |'
echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||~~~~~||~~~'


echo ''
echo -e "${YELLOW}################### DOMAIN INFO ######################${NC}"

### step 1: host
echo ''
echo -e "${BLUE}> host $DOMAIN | tee domain_info.txt ${NC}"
host $DOMAIN | tee domain_info.txt

### step 2: whois
echo ''
echo -e "${BLUE}> whois $DOMAIN | tee -a domain_info.txt${NC}"
whois $DOMAIN | tee -a domain_info.txt # append to domain_info.txt

echo ''
echo -e "${YELLOW}################### SUBDOMAIN BRUTEFORCE ######################${NC}"
mkdir subdomain && cd subdomain

### step 3: amass probed with httpx
echo ''
echo -e "${BLUE}> amass enum -d $DOMAIN > amass.txt ${NC}"
amass enum -d $DOMAIN > amass.txt
echo -e "${BLUE}> cat amass.txt | httpx -random-agent -status-code ${NC}"
cat amass.txt | httpx -random-agent -status-code -title | tee amass_probed_sc.txt
cat amass_probed_sc.txt | cut -d " " -f 1 > amass_probed.txt

cd ..
echo ''
echo -e "${YELLOW}################### ENUMERATION ######################${NC}"
mkdir enumeration && cd enumeration

# step 4: gospider (crawling)
echo ''
echo -e "${BLUE}> gospider -s $FULL_DOMAIN | tee gospider.txt${NC}"
gospider -s $FULL_DOMAIN | tee gospider.txt

# step 5: subdomainizer
echo ''
echo -e "${BLUE}> subdomainizer -u $DOMAIN | tee subdomainizer.txt${NC}"
subdomainizer -u $DOMAIN | tee subdomainizer.txt

cd ..
echo ''
echo -e "${YELLOW}################### SCREENSHOT PAGE ######################${NC}"
cd subdomain

# step 6: eyewitness
echo ''
echo -e "${BLUE}> eyewitness --web -f $(pwd)/amass_probed.txt -d ../screenshot --timeout 60${NC}"
eyewitness --web -f $(pwd)/amass_probed.txt -d ../screenshot --timeout 60
# echo "${RED}screenshot are skipped, please do it manually!${NC}"

cd ..
echo ''
echo -e "${YELLOW}################### VULN SCANNING ######################${NC}"
mkdir vulns && cd vulns

# step 7: nuclei cves
echo ''
echo -e "${BLUE}> nuclei -l ../subdomain/amass_probed.txt -t ~/nuclei-templates/cves | tee cves.txt ${NC}"
nuclei -l ../subdomain/amass_probed.txt -t ~/nuclei-templates/cves | tee cves.txt

# step 8: nuclei vuln
echo ''
echo -e "${BLUE}> nuclei -l ../subdomain/amass_probed.txt -t ~/nuclei-templates/vulnerabilities | tee vulns.txt ${NC}"
nuclei -l ../subdomain/amass_probed.txt -t ~/nuclei-templates/vulnerabilities | tee vulns.txt

cd ..

echo -e "${GREEN}recon on $DOMAIN has finished... ${NC}"
# tnotify "recon on $DOMAIN has finished..."

# isDomainNotValid(){
# }

# todo : subdomain port scanning (massdns)
